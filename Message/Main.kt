import java.io.File

data class Message(val address: String?, val topic: String?, val body: String?)

fun Message.toHTML(): String {
    val template = """
        <html>
            <head>
                <style>
                    /* добавляем стили */
                    table { border-collapse: collapse; }
                    td { border: 1px solid gray; padding: 5px; }
                    .header { font-weight: bold; font-size: 120%; }
                    .body { font-family: sans-serif; }
                </style>
            </head>
            <body>
                <table>
                    ${address?.let { "<tr><td class=\"header\">Address</td><td class=\"body\">$it</td></tr>" } ?: ""}
                    ${topic?.let { "<tr><td class=\"header\">Topic</td><td class=\"body\">$it</td></tr>" } ?: ""}
                    ${body?.let { "<tr><td class=\"header\">Body</td><td class=\"body\">$it</td></tr>" } ?: ""}
                </table>
            </body>
        </html>
    """.trimIndent()

    return template
}

fun main() {
    val message = Message("alexf@igu.ru", "topic", "Pls, i очень want сдать this предмет")

    val html = message.toHTML()

    File("message.html").writeText(html)
}
