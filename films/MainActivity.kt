package com.example.myapplication

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var movieTextView: TextView
    private lateinit var randomMovieButton: Button
    private lateinit var resetButton: Button
    private val movies: MutableList<String> = ArrayList()
    private val moviesUsed: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        movieTextView = findViewById(R.id.movie_text_view)
        randomMovieButton = findViewById(R.id.random_movie_button)
        resetButton = findViewById(R.id.reset_button)

        val movieArray = resources.getStringArray(R.array.movies)
        movieArray.forEach { movie -> movies.add(movie) }

        randomMovieButton.setOnClickListener {
            if (movies.isNotEmpty()) {
                val randomMovie = movies.random()
                movieTextView.text = randomMovie

                movies.remove(randomMovie)
                moviesUsed.add(randomMovie)
            } else {
                movieTextView.text = "Все фильмы уже показаны."
            }
        }

        resetButton.setOnClickListener {
            movies.addAll(moviesUsed)
            moviesUsed.clear()
            movieTextView.text = ""
        }
    }
}
