private val boardSize = 3
private val players = arrayOf("X", "O")
private var currentPlayer = players[0]

private var board = Array(boardSize) { Array(boardSize) { "-" } }

fun main() {
    println("Игра началась!")
    printBoard()
    while (!isGameOver()) {
        println("$currentPlayer, введите номер строки и столбца через пробел:")
        val (row, col) = readLine()!!.split(" ").map { it.toInt() - 1 }
        if (isValidMove(row, col)) {
            placePiece(row, col, currentPlayer)
            printBoard()
            if (isWinner(currentPlayer, board)) {
                println("$currentPlayer выиграл игру!")
                return
            }
            currentPlayer = if (currentPlayer == players[0]) players[1] else players[0]
        } else {
            println("Неправильный ход! Попробуйте еще раз.")
        }
    }
    println("Ничья!")
}

private fun isValidMove(row: Int, col: Int): Boolean {
    return row in 0 until boardSize && col in 0 until boardSize && board[row][col] == "-"
}

private fun placePiece(row: Int, col: Int, player: String) {
    board[row][col] = player
}

private fun isWinner(player: String, board: Array<Array<String>>): Boolean {
    // Проверяем все возможные комбинации для победы
    return (0 until boardSize).any { i ->
        // Проверяем строку
        (0 until boardSize).all { j -> board[i][j] == player } ||
                // Проверяем столбец
                (0 until boardSize).all { j -> board[j][i] == player }
    } || // Проверяем диагонали
            (0 until boardSize).all { i -> board[i][i] == player } ||
            (0 until boardSize).all { i -> board[i][boardSize - i - 1] == player }
}

private fun isGameOver(): Boolean {
    return board.all { row -> row.none { it == "-" } } || isWinner(players[0], board) || isWinner(players[1], board)
}

private fun printBoard() {
    println("   " + (1..boardSize).joinToString(" "))
    board.forEachIndexed { i, row ->
        println("${i + 1} | " + row.joinToString(" "))
    }
}
