package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.myapplication.R

class MainActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private var counter = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = findViewById(R.id.imageView)

        if (savedInstanceState != null) {
            counter = savedInstanceState.getInt("COUNTER")
        }

        setPicture(counter)
    }

    fun onPictureChangeButtonClick() {
        counter++
        if (counter == 4) {
            counter = 1
        }
        setPicture(counter)
    }

    private fun setPicture(counter: Int) {
        when (counter) {
            1 -> imageView.setImageResource(R.drawable.car1)
            2 -> imageView.setImageResource(R.drawable.car2)
            3 -> imageView.setImageResource(R.drawable.car3)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("COUNTER", counter)
    }
}
