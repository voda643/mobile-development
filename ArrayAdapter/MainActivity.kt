package com.example.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private val names by lazy { resources.getStringArray(R.array.names) }
    private val surnames by lazy { resources.getStringArray(R.array.surnames) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val peopleList = generateRandomPeopleList(10)
        val peopleListAdapter = PeopleListAdapter(peopleList)
        recyclerView.adapter = peopleListAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        val buttonAddPerson = findViewById<Button>(R.id.button_add_person)
        buttonAddPerson.setOnClickListener {
            val newPerson = People(names.random(), surnames.random())
            peopleList.add(newPerson)
            peopleListAdapter.notifyDataSetChanged()
        }
    }

    private fun generateRandomPeopleList(count: Int): MutableList<People> {
        val peopleList = mutableListOf<People>()

        repeat(count) {
            val name = names.random()
            val surname = surnames.random()
            peopleList.add(People(name, surname))
        }

        return peopleList
    }

    data class People(val name: String, val surname: String)

    class PeopleListAdapter(
        private val listOfPeople: MutableList<People>
    ) : RecyclerView.Adapter<PeopleListAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_people, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val currentPeople = listOfPeople[position]
            val fullName = currentPeople.name + " " + currentPeople.surname
            holder.tvFullName.text = fullName
        }

        override fun getItemCount(): Int = listOfPeople.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tvFullName: TextView = view.findViewById(R.id.tv_full_name)
        }
    }
}