enum class Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}

class Robot(var x: Int, var y: Int, var direction: Direction) {
    fun turnLeft() {
        direction = when (direction) {
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.LEFT -> Direction.DOWN
            Direction.RIGHT -> Direction.UP
        }
    }

    fun turnRight() {
        direction = when (direction) {
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.LEFT -> Direction.UP
            Direction.RIGHT -> Direction.DOWN
        }
    }

    fun stepForward() {
        when (direction) {
            Direction.UP -> y++
            Direction.DOWN -> y--
            Direction.LEFT -> x--
            Direction.RIGHT -> x++
        }
    }
}

fun moveRobot(robot: Robot, toX: Int, toY: Int) {
    when {
        robot.x < toX -> {
            while (robot.direction != Direction.RIGHT) {
                robot.turnRight()
            }
            while (robot.x < toX) {
                robot.stepForward()
            }
        }
        robot.x > toX -> {
            while (robot.direction != Direction.LEFT) {
                robot.turnLeft()
            }
            while (robot.x > toX) {
                robot.stepForward()
            }
        }
    }

    when {
        robot.y < toY -> {
            while (robot.direction != Direction.UP) {
                robot.turnRight()
            }
            while (robot.y < toY) {
                robot.stepForward()
            }
        }
        robot.y > toY -> {
            while (robot.direction != Direction.DOWN) {
                robot.turnLeft()
            }
            while (robot.y > toY) {
                robot.stepForward()
            }
        }
    }
}
