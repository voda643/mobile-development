package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private lateinit var minValueEditText: EditText
    private lateinit var maxValueEditText: EditText
    private lateinit var startGameButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        minValueEditText = findViewById(R.id.min_value)
        maxValueEditText = findViewById(R.id.max_value)
        startGameButton = findViewById(R.id.start_game_button)

        startGameButton.setOnClickListener {
            val minValue = minValueEditText.text.toString().toIntOrNull()
            val maxValue = maxValueEditText.text.toString().toIntOrNull()

            if (minValue != null && maxValue != null && minValue < maxValue) {
                val intent = Intent(this, GameActivity::class.java)
                intent.putExtra("minValue", minValue)
                intent.putExtra("maxValue", maxValue)
                startActivity(intent)
            }
        }
    }
}
