package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.myapplication.R

class GameActivity : AppCompatActivity() {
    private lateinit var guessTextView: TextView
    private lateinit var lowerButton: Button
    private lateinit var greaterButton: Button

    private var minValue: Int = 0
    private var maxValue: Int = 0
    private var currentGuess: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        minValue = intent.getIntExtra("minValue", 0)
        maxValue = intent.getIntExtra("maxValue", 0)

        guessTextView = findViewById(R.id.guess_text)
        lowerButton = findViewById(R.id.button_lower)
        greaterButton = findViewById(R.id.button_greater)

        updateCurrentGuess()

        lowerButton.setOnClickListener {
            maxValue = currentGuess - 1
            updateCurrentGuess()
        }

        greaterButton.setOnClickListener {
            minValue = currentGuess + 1
            updateCurrentGuess()
        }
    }

    private fun updateCurrentGuess() {
        currentGuess = (minValue + maxValue) / 2
        guessTextView.text = "Мое предположение: $currentGuess"
    }
}
