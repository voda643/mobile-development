import kotlin.math.sqrt

abstract class Figure : FigureInterface, Transforming {
    protected lateinit var sides: FloatArray

    abstract fun getArea(): Float

    override fun printSidesCount() {
        if (!this::sides.isInitialized) return
        println("Count sides of ${figureName}: ${sides.size}")
    }

    override fun printSides() {
        if (!this::sides.isInitialized) return
        print("Sides of ${figureName}:")
        sides.forEach { print(it); print(' ') }
        println()
    }

    override fun move(dx: Float, dy: Float) {
        println("Moving $figureName on [$dx, $dy]")
    }

    override fun rotate(angle: Float) {
        println("Rotating $figureName on $angle degrees")
    }

    override fun scale(k: Float) {
        println("Scaling $figureName with coeff $k")
    }
}

interface FigureInterface {
    val figureName: String

    fun printSidesCount()
    fun printSides()
}

interface Transforming {
    fun move(dx: Float, dy: Float)
    fun rotate(angle: Float)
    fun scale(k: Float)
}

class Rectangle(a: Float, b: Float = -1F) : Figure() {
    init {
        sides = FloatArray(2)
        sides[0] = a
        sides[1] = if (b <= 0) a else b
    }

    constructor(rectangle: Rectangle) : this(rectangle.sides[0], rectangle.sides[1]) {}

    override fun getArea(): Float {
        return sides[0] * sides[1]
    }

    override val figureName: String
        get() = if (sides[0] == sides[1]) "Square" else "Rectangle"
}

class Triangle(a: Float, b: Float, c: Float) : Figure() {

    init {
        sides = FloatArray(3)
        sides[0] = a
        sides[1] = b
        sides[2] = c
    }

    override fun getArea(): Float {
        val p = (sides[0] + sides[1] + sides[2]) / 2
        return sqrt(p * (p - sides[0]) * (p - sides[1]) * (p - sides[2]))
    }

    override val figureName: String
        get() = "Triangle"
}

class Circle(private val r: Float) : Figure() {
    override fun getArea(): Float {
        return Math.PI.toFloat() * r * r
    }

    override fun printSidesCount() {
        println(0)
    }

    override fun printSides() {
        println("Круг не имеет сторон")
    }

    override val figureName: String
        get() = "Circle"

    override fun move(dx: Float, dy: Float) {
        println("Moving $figureName on [$dx, $dy]")
    }

    override fun rotate(angle: Float) {
        println("Rotating $figureName on $angle degrees")
    }

    override fun scale(k: Float) {
        println("Scaling $figureName with coeff $k")
    }
}

fun main() {
    val square = Rectangle(2F)
    println(square.getArea())
    square.printSidesCount()
    square.printSides()

    val rect = Rectangle(3F, 5F)
    println(rect.getArea())

    val triangle = Triangle(4.5F, 5F, 3.65F)
    println(triangle.getArea())
    triangle.printSidesCount()
    triangle.printSides()

    val circle = Circle(5F)
    println(circle.getArea())
    circle.printSidesCount()
    circle.printSides()

    // Transforming
    circle.move(2F, 3F)
    circle.rotate(90F)
    circle.scale(1.5F)

    val rectangle = Rectangle(2F, 3F)
    rectangle.move(1F, 1F)
    rectangle.rotate(45F)
    rectangle.scale(2F)

    val square2 = Rectangle(square)
    square2.move(0.5F, 0.5F)
    square2.rotate(-30F)
    square2.scale(0.5F)
}
