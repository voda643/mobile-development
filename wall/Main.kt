import java.io.File

fun printArr(arr: Array<IntArray>, n: Int, m: Int) {
    for (i in 0 until n) {
        for (j in 0 until m) {
            print(arr[i][j])
        }
        println()
    }
}

fun paintWall(arr: Array<IntArray>, k: Int, n: Int, m: Int) {
    var maxV = 0
    var maxH = 0

    for (i in 0 until n) {
        var counter = 0
        for (j in 0 until m) {
            if (arr[i][j] != 0) {
                counter++
            }
        }
        if (counter > maxH) {
            maxH = counter
        }
    }

    for (i in 0 until m) {
        var counter = 0
        for (j in 0 until n) {
            if (arr[j][i] != 0) {
                counter++
            }
        }
        if (counter > maxV) {
            maxV = counter
        }
    }

    if (k < maxV && k < maxH) {
        println("No")
    } else {
        var l = if (maxH > maxV) maxH else maxV
        for (i in 0 until n) {
            var counter = i
            for (j in 0 until m) {
                if (arr[i][j] != 0) {
                    if (counter >= l) {
                        counter = 0
                    }
                    counter++
                    arr[i][j] = counter
                } else {
                    counter++
                }
            }
        }
        println("Yes")
        printArr(arr, n, m)
    }
}

fun main() {
    val lines = File("test.txt").readLines()
    var c_lines = 0
    val t = lines[c_lines].toInt()

    for (z in 1..t) {
        c_lines++
        val list = lines[c_lines].split(" ")
        val n = list[0].toInt()
        val m = list[1].toInt()
        val k = list[2].toInt()
        val arr = Array(n) { IntArray(m) }
        for (i in 0 until n) {
            c_lines++
            val row = lines[c_lines].split(" ")
            for (j in 0 until m) {
                arr[i][j] = row[j].toInt()
            }
        }
        paintWall(arr, k, n, m)
    }
}
